FROM node:18-alpine AS base

RUN npm i -g pnpm

FROM base AS dependencies

WORKDIR /app
COPY package.json pnpm-lock.yaml ./
RUN pnpm install

FROM base AS build

WORKDIR /app
COPY . .
COPY --from=dependencies /app/node_modules ./node_modules
RUN pnpm build
RUN pnpm prune --prod

FROM base AS deploy

ENV DOCKER_GATEWAY host.docker.internal

ENV APP_ENV=
ENV CACHE_RETENTION_PERIOD_MINUTES=
ENV APP_AWS_ACCESS_KEY_ID=
ENV APP_AWS_SECRET_ACCESS_KEY=
ENV CACHE_BUCKET_NAME=
ENV CACHE_ENABLED=

WORKDIR /app
COPY --from=build /app/dist/ ./dist/
COPY --from=build /app/node_modules ./node_modules

CMD [ "node", "dist/main.js" ]