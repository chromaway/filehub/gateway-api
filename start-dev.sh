URL=https://node0.devnet2.chromia.dev:7740
FILEHUB_BRID=$(curl "${URL}/brid/iid_100" -s)

cat > src/persistence/data/filehubs.json <<- EOM
{
  "${FILEHUB_BRID}": {
    "brid": "${FILEHUB_BRID}",
    "url": "${URL}"
  },
  "local": {
    "brid": "${FILEHUB_BRID}",
    "url": "${URL}",
    "alias": "local"
  }
}
EOM

npm run start:debug
