# Filehub Gateway API

## Usage

Gateway API uses a simple JSON file to retreive details (BRID, Url) about configured Filehubs. In order to retrieve files from Filehub through the Gateway API the JSON file must be updated.

##### _devnet example_

```json
{
  "E3795BCAFBF9008698B0EA1A83CE2E82680F4E05B7FDE3985DDF49252868D43E": {
    "brid": "E3795BCAFBF9008698B0EA1A83CE2E82680F4E05B7FDE3985DDF49252868D43E",
    "url": "https://node0.devnet1.chromia.dev:7740"
  },
  "filehub-v2": {
    "brid": "E3795BCAFBF9008698B0EA1A83CE2E82680F4E05B7FDE3985DDF49252868D43E",
    "url": "https://node0.devnet1.chromia.dev:7740",
    "alias": "filehub-v2"
  }
}
```

##### _local example_

```json
{
  "83E4F014E9DE987849D8010C325F372BBBBF164397D5C3EB0F92D29CE25359D4": {
    "brid": "83E4F014E9DE987849D8010C325F372BBBBF164397D5C3EB0F92D29CE25359D4",
    "url": "http://localhost:7740"
  },
  "local": {
    "brid": "D18E670A988E1F7939B5F86F94775E8E95C2AF7E74C53A7CF0881ED938A38C2D",
    "url": "http://localhost:7740",
    "alias": "local"
  }
}
```

## Endpoints

Gateway API has one endpoint to retrieve files from Filehub.

#### Reading file from Filehub

<details>
 <summary><code>GET</code> <code><b>/{bridOrAlias}/{fileHash}</b></code> <code>(retrieves file by it's hash from specific Filehub)</code></summary>

##### Parameters

> | name        | type     | data type | description              |
> | ----------- | -------- | --------- | ------------------------ |
> | bridOrAlias | required | string    | name or brid for Filehub |
> | filehash    | required | string    | hash of file             |

##### Responses

> | http code | content-type                     | response                                                                                                                                              |
> | --------- | -------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
> | `304`     | File content type                | File content                                                                                                                                          |
> | `404`     | `application/json;charset=utf-8` | `{"message":"File with hash [just-for-docs] could not be found on Filehub with BRID or alias [filehub-v2]","error":"Not Found","statusCode":404}`None |

##### Example cURL

> ```javascript
>  curl -X GET https://filehub-gateway-api.dev.chromia.dev/filehub-v2/92644e13c174c23d763d9b8659ecab65893af44ff7668476dc5b4c3350ac84a5
> ```

</details>

---

### Example

Try these examples for reference:

1. https://filehub-gateway-api.dev.chromia.dev/filehub-v2/92644e13c174c23d763d9b8659ecab65893af44ff7668476dc5b4c3350ac84a5
2. https://filehub-gateway-api.dev.chromia.dev/E3795BCAFBF9008698B0EA1A83CE2E82680F4E05B7FDE3985DDF49252868D43E/92644e13c174c23d763d9b8659ecab65893af44ff7668476dc5b4c3350ac84a5
