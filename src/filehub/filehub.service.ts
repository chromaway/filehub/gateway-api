import { Injectable } from '@nestjs/common';
import { FilehubConfig } from './../persistence/data/Filehub';
import { Readable } from 'stream';
import { Filehub, FsFile, FsFileMetadata } from 'filehub';

@Injectable()
export class FilehubService {
  async get(
    filehubConfig: FilehubConfig,
    hash: string,
  ): Promise<FsFile | undefined> {
    try {
      console.log(
        `Fetch file with hash [${hash}] from filehub: ${filehubConfig.brid}:${filehubConfig.url}`,
      );

      const filehub = new Filehub({
        directoryNodeUrlPool: filehubConfig.url.split(','),
        blockchainRid: filehubConfig.brid,
      });

      const file = await filehub.getFile(Buffer.from(hash, 'hex'));

      return file;
    } catch (err: any) {
      console.error(err.message);
      return;
    }
  }

  async getMetadata(
    filehubConfig: FilehubConfig,
    hash: string,
  ): Promise<FsFileMetadata | undefined> {
    try {
      const filehub = new Filehub({
        directoryNodeUrlPool: filehubConfig.url,
        blockchainRid: filehubConfig.brid,
      });

      const metadata = await filehub.getFileMetadata(Buffer.from(hash, 'hex'));
      if (!metadata) return;

      return JSON.parse(metadata.toString());
    } catch (err: any) {
      console.error(err.message);
      return;
    }
  }

  async getFileStream(
    filehubConfig: FilehubConfig,
    hash: string,
    start: number = 0,
    end: number = -1,
  ): Promise<Readable> {
    const filehub = new Filehub({
      directoryNodeUrlPool: filehubConfig.url,
      blockchainRid: filehubConfig.brid,
    });

    return filehub.getFileStream(Buffer.from(hash, 'hex'), { 
      batchSize: 5, 
      start,
      end
    });
  }
}
