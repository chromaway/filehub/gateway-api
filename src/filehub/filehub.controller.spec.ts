import { Test, TestingModule } from '@nestjs/testing';
import { FilehubController } from './filehub.controller';
import { FilehubService } from './filehub.service';
import { PersistenceService } from '../persistence/persistence.service';

describe('FilehubController', () => {
  let controller: FilehubController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FilehubController],
      providers: [FilehubService, PersistenceService],
    }).compile();

    controller = module.get<FilehubController>(FilehubController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
