import { Module } from '@nestjs/common';
import { FilehubController } from './filehub.controller';
import { FilehubService } from './filehub.service';
import { PersistenceModule } from './../persistence/persistence.module';
import { CacheModule } from '../cache/cache.module';

@Module({
  controllers: [FilehubController],
  providers: [FilehubService],
  imports: [PersistenceModule, CacheModule],
})
export class FilehubModule {}
