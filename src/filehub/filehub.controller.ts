import { Controller, Get, NotFoundException, Param, Res, Headers, Logger, UnauthorizedException, Post, Body } from '@nestjs/common';
import { FilehubService } from './filehub.service';
import { PersistenceService } from './../persistence/persistence.service';
import { Response } from 'express';
import { CacheService } from '../cache/cache.service';
import { ConfigService } from '@nestjs/config';

function formatLogMessage(hash: string, msg: string): string {
  return `[${hash}]: ${msg}`;
}

@Controller('/')
export class FilehubController {
  private readonly logger = new Logger(FilehubController.name);
  private readonly apiToken = this.configService.get<string>('API_TOKEN');
  private DEFAULT_CONTENT_TYPE = 'application/octet-stream';

  constructor(
    private readonly service: FilehubService,
    private readonly Persistence: PersistenceService,
    private readonly cacheService: CacheService,
    private readonly configService: ConfigService,
  ) {
    this.apiToken = this.configService.get<string>('API_TOKEN');
  }

  @Post('clear-cache')
  async clearCache(
    @Body() body: { hash: string },
    @Headers('x-api-token') apiToken: string
  ) {
    if (apiToken !== this.apiToken) {
      this.logger.warn(formatLogMessage(body.hash, 'Unauthorized attempt to clear cache'));
      throw new UnauthorizedException('Invalid API token');
    }
    await this.cacheService.clearCache(body.hash);
    this.logger.log(formatLogMessage(body.hash, `Cache cleared for hash: ${body.hash}`));
    return { message: 'Cache cleared successfully' };
  }

  @Get(':bridOrAlias/:hash')
  async get(
    @Param() params: { bridOrAlias: string; hash: string },
    @Headers() headers: Record<string, string>,
    @Res() res: Response,
  ) {
    const filehub = await this.getFilehubOrThrow(params.bridOrAlias);
    const metadata = await this.getMetadataOrThrow(filehub, params.bridOrAlias, params.hash);
    const { start, end, contentLength } = this.parseRangeHeader(headers.range, metadata);

    this.setResponseHeaders(res, metadata, start, end, contentLength);

    const stream = await this.getFileStream(filehub, params.hash, start, end, contentLength);
    await this.streamFileToResponse(params.hash, stream, res, start, end);
  }

  private async getFilehubOrThrow(bridOrAlias: string): Promise<any> {
    const filehub = this.Persistence.getFilehub(bridOrAlias);
    if (!filehub) {
      this.logger.log(formatLogMessage(bridOrAlias, `Filehub not found for BRID/Alias: ${bridOrAlias}`));
      throw new NotFoundException(
        `Configured Filehub with BRID or alias [${bridOrAlias}] could not be found.`,
      );
    }
    return filehub;
  }

  private async getMetadataOrThrow(filehub: any, bridOrAlias: string, hash: string): Promise<any> {
    let metadata = await this.cacheService.getMetadata(hash);
    if (!metadata) {
      metadata = await this.service.getMetadata(filehub, hash);
      if (!metadata) {
        this.logger.debug(formatLogMessage(hash, `File not found for Hash: ${hash}`));
        throw new NotFoundException(
          `File with hash [${hash}] could not be found on Filehub with BRID or alias [${bridOrAlias}]`,
        );
      }
      await this.cacheService.setMetadata(hash, metadata);
    }
    return metadata;
  }

  private parseRangeHeader(range: string | undefined, metadata: any): { start: number; end: number; contentLength: number } {
    const contentLength = parseInt(metadata['Content-Length'], 10) || 0;
    let start = 0;
    let end = contentLength - 1;

    if (range) {
      const parts = range.replace(/bytes=/, "").split("-");
      start = parseInt(parts[0], 10);
      end = parts[1] ? parseInt(parts[1], 10) : Math.min(start + 2500000, contentLength - 1);

      if (isNaN(start)) start = 0;
      if (isNaN(end) || end >= contentLength) end = contentLength - 1;
      if (start > end) throw new Error('Requested range not satisfiable');
    }

    return { start, end, contentLength };
  }

  private setResponseHeaders(res: Response, metadata: any, start: number, end: number, contentLength: number): void {
    const contentType = metadata['Content-Type'] || this.DEFAULT_CONTENT_TYPE;
    res.header('Content-Type', contentType);
    if (metadata['Content-Encoding']) {
      res.header('Content-Encoding', metadata['Content-Encoding']);
    }

    if (start !== 0 || end !== contentLength - 1) {
      res.status(206);
      res.header('Content-Range', `bytes ${start}-${end}/${contentLength}`);
    }

    const chunksize = (end - start) + 1;
    res.header('Accept-Ranges', 'bytes');
    res.header('Cache-Control', 'public, max-age=3600');
    res.header('Content-Disposition', 'inline');
    res.header('Content-Length', chunksize.toString());
    res.header('X-Content-Type-Options', 'nosniff');
    res.header('X-Download-Options', 'noopen');
    res.header('X-File-Warning', 'This file is from an external source. Be cautious when opening.');

    // Conditional sandbox permissions and CSP
    if (contentType.startsWith('text/html') || contentType.startsWith('video/')) {
      res.header('Content-Security-Policy', "default-src 'none'; media-src 'self'; sandbox allow-scripts");
    } else {
      res.header('Content-Security-Policy', "default-src 'none'; media-src 'self'; sandbox");
    }
  }

  private async getFileStream(filehub: any, hash: string, start: number, end: number, contentLength: number): Promise<any> {
    let stream = await this.cacheService.getFileStream(hash, start, end);
    if (!stream) {
      this.logger.debug(formatLogMessage(hash, 'Stream not found in cache, fetching from filehub'));
      stream = await this.service.getFileStream(filehub, hash, start, end);

      this.triggerBackgroundCaching(hash, filehub, contentLength);
    }
    return stream;
  }

  private triggerBackgroundCaching(hash: string, filehub: any, contentLength: number): void {
    this.cacheService.cacheEntireFile(hash, filehub, contentLength)
      .catch(error => this.logger.error(formatLogMessage(hash, `Error initiating background caching: ${error.message}`)));
  }

  private async streamFileToResponse(hash: string, stream: any, res: Response, start: number, end: number): Promise<void> {
    let bytesSent = 0;
    const totalBytes = end - start + 1;
    let isEnding = false;
    let timeout: NodeJS.Timeout;

    const startTime = Date.now();
    let lastChunkTime = startTime;
    let firstChunkTime: number | null = null;

    this.logger.log(formatLogMessage(hash, `Starting stream. Total bytes to send: ${totalBytes}`));

    const cleanup = () => {
      if (isEnding) return;
      isEnding = true;
      stream.destroy();
      res.end();
      clearTimeout(timeout); // Ensure the timeout is cleared
      this.logger.log(formatLogMessage(hash, `Cleanup called. Bytes sent: ${bytesSent}/${totalBytes}`));
    };

    const handleData = (chunk: Buffer) => {
      if (isEnding) return;

      const currentTime = Date.now();
      if (firstChunkTime === null) {
        firstChunkTime = currentTime;
        this.logger.debug(formatLogMessage(hash, `Time to first chunk returned: ${firstChunkTime - startTime}ms`));
      } else {
        this.logger.debug(formatLogMessage(hash, `Time between chunks: ${currentTime - lastChunkTime}ms`));
      }
      lastChunkTime = currentTime;

      const remainingBytes = totalBytes - bytesSent;
      const chunkToSend = remainingBytes < chunk.length ? chunk.slice(0, remainingBytes) : chunk;

      if (chunkToSend.length > 0) {
        res.write(chunkToSend);
        bytesSent += chunkToSend.length;
        this.logger.debug(formatLogMessage(hash, `Chunk sent. Size: ${chunkToSend.length}, Total sent: ${bytesSent}/${totalBytes}`));
      }

      if (bytesSent >= totalBytes) {
        this.logger.debug(formatLogMessage(hash, `All bytes sent. Calling cleanup.`));
        clearTimeout(timeout); // Clear the timeout when all bytes are sent
        cleanup();
      }

      // Reset the timeout on each chunk received
      clearTimeout(timeout);
      timeout = setStreamTimeout(hash, cleanup, bytesSent, totalBytes);
    };

    const setStreamTimeout = (hash: string, cleanup: () => void, bytesSent: number, totalBytes: number): NodeJS.Timeout => {
      return setTimeout(() => {
        this.logger.warn(formatLogMessage(hash, `Stream timeout reached. Forcing cleanup. Bytes sent: ${bytesSent}/${totalBytes}`));
        cleanup();
      }, 60000); // Increased timeout to 60 seconds
    };

    stream.on('data', handleData);
    stream.on('end', () => {
      this.logger.debug(formatLogMessage(hash, `Stream 'end' event. Total bytes sent: ${bytesSent}/${totalBytes}`));
      clearTimeout(timeout); // Clear the timeout when the stream ends
      cleanup();
    });
    stream.on('error', (error) => {
      this.logger.error(formatLogMessage(hash, `Stream error: ${error.message}`));
      clearTimeout(timeout); // Clear the timeout on error
      cleanup();
    });
    res.on('close', () => {
      const endTime = Date.now();
      this.logger.debug(formatLogMessage(hash, `Response 'close' event. Stream destroyed: ${stream.destroyed}, Bytes sent: ${bytesSent}/${totalBytes}`));
      this.logger.log(formatLogMessage(hash, `Time to whole file returned: ${endTime - startTime}ms`));
      clearTimeout(timeout); // Clear the timeout on response close
      cleanup();
    });

    timeout = setStreamTimeout(hash, cleanup, bytesSent, totalBytes);
  }
}
