import { Test, TestingModule } from '@nestjs/testing';
import { FilehubService } from './filehub.service';

describe('FilehubService', () => {
  let service: FilehubService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FilehubService],
    }).compile();

    service = module.get<FilehubService>(FilehubService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
