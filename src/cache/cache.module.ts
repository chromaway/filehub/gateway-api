import { Module } from '@nestjs/common';
import { CacheService } from './cache.service';
import { PersistenceModule } from 'src/persistence/persistence.module';

@Module({
  imports: [PersistenceModule],
  providers: [CacheService],
  exports: [CacheService],
})
export class CacheModule {}