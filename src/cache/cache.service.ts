import { Injectable, OnModuleInit, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Filehub } from 'filehub';
import { S3Client, HeadBucketCommand, CreateBucketCommand, HeadObjectCommand, PutObjectCommand, GetObjectCommand, ListObjectsV2Command, DeleteObjectCommand } from '@aws-sdk/client-s3';
import { FilehubConfig } from 'src/persistence/data/Filehub';
import { Readable } from 'stream';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class CacheService implements OnModuleInit {
  private readonly logger = new Logger(CacheService.name);
  private readonly retentionPeriodMinutes: number;
  private readonly s3Client: S3Client;
  private readonly bucketName: string;
  private readonly enabled: boolean;
  private readonly locks = new Map<string, boolean>();

  constructor(
    private readonly configService: ConfigService,
  ) {
    this.enabled = this.configService.get<string>('CACHE_ENABLED') === 'true';
    if (!this.enabled) {
      this.logger.warn('Cache is disabled');
      return;
    }

    this.s3Client = new S3Client({
      region: this.configService.get<string>('APP_AWS_REGION'),
      credentials: {
        accessKeyId: this.configService.get<string>('APP_AWS_ACCESS_KEY_ID'),
        secretAccessKey: this.configService.get<string>('APP_AWS_SECRET_ACCESS_KEY'),
      },
      forcePathStyle: true,
    });
    this.retentionPeriodMinutes = this.configService.get<number>('CACHE_RETENTION_PERIOD_MINUTES', 1);
    this.bucketName = this.configService.get<string>('CACHE_BUCKET_NAME');
  }

  async onModuleInit() {
    if (!this.enabled) {
      return;
    }

    try {
      await this.s3Client.send(new HeadBucketCommand({ Bucket: this.bucketName }));
    } catch (error) {
      if (error.name === 'NotFound') {
        await this.s3Client.send(new CreateBucketCommand({ Bucket: this.bucketName }));
        this.logger.log(`Created bucket: ${this.bucketName}`);
      } else {
        throw error;
      }
    }
  }

  async getMetadata(hash: string): Promise<any | null> {
    if (!this.enabled) {
      return null;
    }

    try {
      const metadataObj = await this.s3Client.send(new HeadObjectCommand({
        Bucket: this.bucketName,
        Key: this.getMetadataKey(hash),
      }));
      const metadata = JSON.parse(metadataObj.Metadata['x-amz-meta-metadata']);
      
      // Update last accessed time
      metadata.lastAccessed = new Date().toISOString();
      await this.setMetadata(hash, metadata);
      
      return metadata;
    } catch (err) {
      return null;
    }
  }

  async setMetadata(hash: string, metadata: any): Promise<void> {
    if (!this.enabled) {
      return;
    }

    // Ensure lastAccessed is set
    if (!metadata.lastAccessed) {
      metadata.lastAccessed = new Date().toISOString();
    }
    
    const metadataString = JSON.stringify(metadata);
    const key = this.getMetadataKey(hash);
    await this.s3Client.send(new PutObjectCommand({
      Bucket: this.bucketName,
      Key: key,
      Body: Buffer.from(metadataString),
      ContentType: 'application/json',
      Metadata: {
        'x-amz-meta-metadata': metadataString,
      },
    }));

    this.logger.debug(`Stored metadata for ${key}`);
  }

  async getFileStream(hash: string, start: number, end: number): Promise<Readable | null> {
    if (!this.enabled) {
      return null;
    }

    try {
      const params = {
        Bucket: this.bucketName,
        Key: hash,
        Range: `bytes=${start}-${end}`,
      };
      const data = await this.s3Client.send(new GetObjectCommand(params));
      return data.Body as Readable;
    } catch (err) {
      return null;
    }
  }

  async setFile(hash: string, stream: Readable, size: number): Promise<void> {
    if (!this.enabled) {
      return;
    }

    try {
      await this.s3Client.send(new PutObjectCommand({
        Bucket: this.bucketName,
        Key: hash,
        Body: stream,
        ContentLength: size,
      }));
    } catch (error) {
      this.logger.error(`Error setting file in cache: ${error.message}`);
      throw error;
    }
  }

  async cacheEntireFile(hash: string, filehubConfig: FilehubConfig, size: number): Promise<void> {
    if (!this.enabled) {
      return;
    }

    if (!this.acquireLock(hash)) {
      this.logger.warn(`Cache lock for ${hash} is already acquired`);
      return;
    }

    try {
      const filehub = new Filehub({
        directoryNodeUrlPool: filehubConfig.url,
        blockchainRid: filehubConfig.brid,
      });
      this.logger.log(`Starting to cache the file from filehub: ${hash}`);
      const stream = await filehub.getFileStream(Buffer.from(hash, 'hex'));
      await this.s3Client.send(new PutObjectCommand({
        Bucket: this.bucketName,
        Key: hash,
        Body: stream,
        ContentLength: size,
      }));
      this.logger.log(`Cached entire file: ${hash}`);
    } catch (error) {
      this.logger.error(`Error caching entire file: ${error.message}`);
    } finally {
      this.releaseLock(hash);
    }
  }

  async removeOldFiles(): Promise<void> {
    if (!this.enabled) {
      return;
    }

    const thirtyDaysAgo = new Date();
    thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30);

    const listParams = {
      Bucket: this.bucketName,
    };

    const objects = await this.s3Client.send(new ListObjectsV2Command(listParams));
    for (const obj of objects.Contents) {
      if (obj.LastModified < thirtyDaysAgo) {
        await this.s3Client.send(new DeleteObjectCommand({
          Bucket: this.bucketName,
          Key: obj.Key,
        }));
        this.logger.log(`Removed old object: ${obj.Key}`);
      }
    }
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async removeUnusedFiles(): Promise<void> {
    if (!this.enabled) {
      return;
    }

    const cutoffDate = new Date();
    cutoffDate.setMinutes(cutoffDate.getMinutes() - this.retentionPeriodMinutes);

    const listParams = {
      Bucket: this.bucketName,
    };

    const objects = await this.s3Client.send(new ListObjectsV2Command(listParams));
    for (const obj of objects.Contents) {
      if (obj.Key.startsWith('metadata-')) {
        try {
          const metadataObj = await this.s3Client.send(new HeadObjectCommand({
            Bucket: this.bucketName,
            Key: obj.Key,
          }));
          const metadataString = metadataObj.Metadata['x-amz-meta-metadata'];
          
          if (!metadataString) {
            this.logger.warn(`No metadata found for object: ${obj.Key}`);
            continue;
          }

          const metadata = JSON.parse(metadataString);
          
          if (!metadata.lastAccessed) {
            this.logger.warn(`No lastAccessed timestamp for object: ${obj.Key}`);
            continue;
          }

          if (new Date(metadata.lastAccessed) < cutoffDate) {
            const fileHash = obj.Key.replace('metadata-', '');
            await this.s3Client.send(new DeleteObjectCommand({
              Bucket: this.bucketName,
              Key: obj.Key,
            }));
            await this.s3Client.send(new DeleteObjectCommand({
              Bucket: this.bucketName,
              Key: fileHash,
            }));
            this.logger.log(`Removed unused file and metadata: ${fileHash}`);
          }
        } catch (error) {
          this.logger.error(`Error processing object ${obj.Key}: ${error.message}`);
        }
      }
    }
  }

  async clearCache(hash: string): Promise<void> {
    if (!this.enabled) {
      return;
    }

    try {
      await this.s3Client.send(new DeleteObjectCommand({
        Bucket: this.bucketName,
        Key: hash,
      }));
      
      const metadataKey = this.getMetadataKey(hash);
      await this.s3Client.send(new DeleteObjectCommand({
        Bucket: this.bucketName,
        Key: metadataKey,
      }));

      this.logger.log(`Cleared cache for hash: ${hash}`);
    } catch (error) {
      // If the object doesn't exist, it's not an error
      if (error.name !== 'NoSuchKey') {
        this.logger.error(`Error clearing cache for hash ${hash}: ${error.message}`);
        throw error;
      }
    }
  }

  getMetadataKey(hash: string): string {
    return `metadata-${hash}`;
  }

  // Method to acquire a lock
  private acquireLock(hash: string): boolean {
    if (this.locks.get(hash)) {
      return false; // Lock already acquired
    }
    this.locks.set(hash, true);
    return true;
  }

  // Method to release a lock
  private releaseLock(hash: string): void {
    this.locks.delete(hash);
  }
}