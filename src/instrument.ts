import * as Sentry from "@sentry/nestjs"
import { nodeProfilingIntegration } from "@sentry/profiling-node";

Sentry.init({
  dsn: process.env.SENTRY_DSN,
  integrations: [
    nodeProfilingIntegration(),
  ],
  environment: process.env.APP_ENV,
  tracesSampleRate: 1,
  profilesSampleRate: 1.0,
});