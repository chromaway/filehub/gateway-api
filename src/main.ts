import "./instrument.ts";

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, LogLevel } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);

  // Get the log level from environment variables
  const logLevel = configService.get<string>('LOG_LEVEL', 'log');

  // Define a mapping from a single log level to the appropriate array of log levels
  const logLevelMap: { [key in LogLevel]: LogLevel[] } = {
    fatal: ['fatal'],
    error: ['fatal', 'error'],
    warn: ['fatal', 'error', 'warn'],
    log: ['fatal', 'error', 'warn', 'log'],
    debug: ['fatal', 'error', 'warn', 'log', 'debug'],
    verbose: ['fatal', 'error', 'warn', 'log', 'debug', 'verbose'],
  };

  // Get the appropriate array of log levels
  const logLevels = logLevelMap[logLevel as LogLevel] || logLevelMap['log'];

  // Set the global log level
  Logger.overrideLogger(logLevels);

  await app.listen(3000);
}
bootstrap();
