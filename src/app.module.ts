import { Module } from '@nestjs/common';
import { HealthModule } from './health/health.module';
import { FilehubModule } from './filehub/filehub.module';
import { PersistenceModule } from './persistence/persistence.module';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigModule } from '@nestjs/config';
import { SentryModule } from '@sentry/nestjs/setup';

@Module({
  imports: [
    SentryModule.forRoot(),
    HealthModule,
    FilehubModule,
    PersistenceModule,
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env',
  })],
  controllers: [],
  providers: [],
})
export class AppModule { }
