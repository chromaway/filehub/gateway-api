export interface FilehubConfig {
  url: string;
  alias?: string;
  brid: string;
}

export interface Filehubs {
  [index: string]: FilehubConfig;
}
