import { Inject, Injectable } from '@nestjs/common';
import * as filehubs from './data/filehubs.json';
import { FilehubConfig, Filehubs } from './data/Filehub';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class PersistenceService {
  private readonly filehubs: Filehubs;

  constructor(@Inject(ConfigService) private readonly configService: ConfigService) {
    this.filehubs = filehubs[this.configService.get('APP_ENV')];
  }

  public getFilehub(bridOrAlias: string): FilehubConfig | null {
    return this.filehubs[bridOrAlias];
  }
}
